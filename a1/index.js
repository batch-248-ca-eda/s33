


	// 1
	
	fetch ('http://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((data) => {

		let list = data.map((todo =>{
			return todo.title
		}))

		console.log(list)

	});



	// 2

	fetch ('https://jsonplaceholder.typicode.com/todos/1')
			.then((response) => response.json())
			.then((data) => console.log(`The Item "${data.title}" on the list has a status of ${data.completed}`));



	// 3

	fetch('https://jsonplaceholder.typicode.com/todos',{
				method:'POST',
				headers:{
					'Content-type':'application/json'
				},
				body: JSON.stringify({
					title: "Create To Do list Item",
					completed: false,
					userId: 1,
					id: 201
				})
			})
			.then((response) => response.json())
			.then((data) => console.log(data))



	// 4

	fetch('https://jsonplaceholder.typicode.com/todos/1',{
				method:'PUT',
				headers:{
					'Content-type':'application/json'
				},
				body: JSON.stringify({
					title: "Update To Do list Item",
					description: "To update the my to do list with a different data structure",
					status: "Pending",
					dateCompleted: "Pending",
					userId: 1
				})
			})
			.then((response) => response.json())
			.then((data) => console.log(data))



	// 5

	fetch('https://jsonplaceholder.typicode.com/todos/1',{
				method:'PATCH',
				headers:{
					'Content-type':'application/json'
				},
				body: JSON.stringify({
					completed: false,
					dateCompleted: "07/09/21",
					id: 1,
					status: "Complete",
					title: "delectus aut autem",
					userId: 1
				})
			})
			.then((response) => response.json())
			.then((data) => console.log(data))



	// 6

	fetch('https://jsonplaceholder.typicode.com/todos/1',{
				method: "DELETE"
			})
	.then((response)=>response.json())
	.then((data)=>console.log(data));